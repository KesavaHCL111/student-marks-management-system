package com.college.util;

public class Query {
	public static String lecturerAuth = "select * from lecturer where uid=? and password=?";
	public static String viewMarks = "select * from student_marks";
	public static String addStudent = "insert into student_marks values(?,?,?,?,?)";
	public static String editName = "update student_marks set sname=? where sid=?";
	public static String editJavaSqlMarks = "update student_marks set java_Marks=?,sql_Marks=? where sid=?";
	public static String editSqlHtmlMarks = "update student_marks set sql_Marks=?,html_Marks=? where sid=?";
	public static String editAllSubjectMarks = "update student_marks set java_Marks=?,sql_Marks=?,html_Marks=? where sid=?";
	public static String removeStudent="delete from student_marks where sid=? ";
	public static String changePassword="update lecturer set password=? where uid=?";
}
