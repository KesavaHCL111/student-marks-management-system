package com.college.dao;

import java.util.List;

import com.college.model.Lecturer;
import com.college.model.StudentMarks;

public interface LecturerDao {
	public int lecturerAuthentication(Lecturer lecturer);
	public List<StudentMarks> viewStudentMarks();
	public int addStudent(StudentMarks student);
	public int editName(StudentMarks student);
	public int editJavaSqlMarks(StudentMarks student);
	public int editSqlHtmlMarks(StudentMarks student);
	public int editAllSubjectMarks(StudentMarks student);
	public int removeStudent(StudentMarks student);
	public int changePassword(Lecturer lecturer);
	

}
