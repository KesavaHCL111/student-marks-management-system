package com.college.controller;

import java.util.List;

import com.college.dao.LecturerDao;
import com.college.dao.LecturerImpl;
import com.college.model.Lecturer;
import com.college.model.StudentMarks;

public class LecturerController {
	int result;
	LecturerDao dao = new LecturerImpl();

	public int lecturerAuthentication(String uid, String password) {

		Lecturer lecturer = new Lecturer(uid, password);
		return dao.lecturerAuthentication(lecturer);

	}

	public List<StudentMarks> viewStudentMarks() {
		return dao.viewStudentMarks();
	}

	public int addStudent(int sid, String sname, int java_Marks, int sql_Marks, int html_Marks) {
		StudentMarks student = new StudentMarks(sid, sname, java_Marks, sql_Marks, html_Marks);
		return dao.addStudent(student);
	}

	public int editName(int sid, String sname) {
		StudentMarks student = new StudentMarks();
		student.setSid(sid);
		student.setSname(sname);
		return dao.editName(student);

	}

	public int editJavaSqlMarks(int sid, int java_Marks, int sql_Marks) {
		StudentMarks student = new StudentMarks();
		student.setSid(sid);
		student.setJava_Marks(java_Marks);
		student.setSql_Marks(sql_Marks);
		return dao.editJavaSqlMarks(student);

	}

	public int editSqlHtmlMarks(int sid, int sql_Marks, int html_Marks) {
		StudentMarks student = new StudentMarks();
		student.setSid(sid);
		student.setSql_Marks(sql_Marks);
		student.setHtml_Marks(html_Marks);
		return dao.editSqlHtmlMarks(student);
	}

	public int editAllSubjectMarks(int sid, int java_Marks, int sql_Marks, int html_Marks) {
		StudentMarks student = new StudentMarks();
		student.setSid(sid);
		student.setJava_Marks(java_Marks);
		student.setSql_Marks(sql_Marks);
		student.setHtml_Marks(html_Marks);
		return dao.editAllSubjectMarks(student);

	}

	public int removeStudent(int sid) {
		StudentMarks student = new StudentMarks();
		student.setSid(sid);
		return dao.removeStudent(student);
	}

	public int changePassword(String uid, String password) {
		Lecturer lecturer = new Lecturer(uid, password);
		return dao.changePassword(lecturer);

	}

}
