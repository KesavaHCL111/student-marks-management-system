package com.college.view;

import java.util.List;
import java.util.Scanner;

import com.college.controller.LecturerController;
import com.college.model.StudentMarks;

public class Main {

	public static void main(String[] args) {

		Scanner s = new Scanner(System.in);
		System.out.println("Enter User id");
		String uid = s.nextLine();
		System.out.println("Enter password");
		String password = s.nextLine();
		LecturerController controller = new LecturerController();
		int result = 0;
		result = controller.lecturerAuthentication(uid, password);
		if (result > 0) {
			System.out.println("Hi " + uid + " Welcome to Lecturer Page");
			int cont;
			do {
				System.out.println("1)Add Student 2)Reomve Student 3)Update Student 4)View All Student");
				int option = s.nextInt();
				int sid = 0;
				String sname = "";
				int javaMarks = 0;
				int sqlMarks = 0;
				int htmlMarks = 0;
				switch (option) {
				case 1:
					System.out.println("Enter Student Id");
					sid = s.nextInt();
					s.nextLine();
					System.out.println("Enter Student Name");
					sname = s.nextLine();
					System.out.println("Enter Java Marks");
					javaMarks = s.nextInt();
					System.out.println("Enter Sql Marks");
					sqlMarks = s.nextInt();
					System.out.println("Enter Html Marks");
					htmlMarks = s.nextInt();
					result = controller.addStudent(sid, sname, javaMarks, sqlMarks, htmlMarks);
					if (result > 0) {
						System.out.println(sid + " Added Succesfully");
					}
					break;
				case 2:
					System.out.println("Enter Student Id for remove");
					sid = s.nextInt();
					result = controller.removeStudent(sid);
					System.out.println((result > 0) ? sid + " Remove Successfully" : sid + " Remove UnSuccessfully");
					break;
				case 3:
					System.out.println(
							"1]Edit Name 2]Edit Java&Sql marks  3]Edit Sql&Html marks  4]Edit Java,Sql&Html marks");
					option = s.nextInt();
					if (option == 1) {
						System.out.println("Enter Student Id");
						sid = s.nextInt();
						s.nextLine();
						System.out.println("Enter Student Name");
						sname = s.nextLine();
						result = controller.editName(sid, sname);
						System.out.println(
								(result > 0) ? sid + " Updated Successfully" : sid + " Not Updated Successfully");

					} else if (option == 2) {
						System.out.println("Enter Student Id");
						sid = s.nextInt();
						System.out.println("Enter Java Marks");
						javaMarks = s.nextInt();
						System.out.println("Enter Sql Marks");
						sqlMarks = s.nextInt();
						result = controller.editJavaSqlMarks(sid, javaMarks, sqlMarks);
						System.out.println(
								(result > 0) ? sid + " Updated Successfully" : sid + " Not Updated Successfully");

					} else if (option == 3) {
						System.out.println("Enter Student Id");
						sid = s.nextInt();
						System.out.println("Enter Sql Marks");
						sqlMarks = s.nextInt();
						System.out.println("Enter Html Marks");
						htmlMarks = s.nextInt();
						result = controller.editSqlHtmlMarks(sid, sqlMarks, htmlMarks);
						System.out.println(
								(result > 0) ? sid + " Updated Successfully" : sid + " Not Updated Successfully");

					} else if (option == 4) {
						System.out.println("Enter Student Id");
						sid = s.nextInt();
						System.out.println("Enter Java Marks");
						javaMarks = s.nextInt();
						System.out.println("Enter Sql Marks");
						sqlMarks = s.nextInt();
						System.out.println("Enter Html Marks");
						htmlMarks = s.nextInt();
						result = controller.editAllSubjectMarks(sid, javaMarks, sqlMarks, htmlMarks);
						System.out.println(
								(result > 0) ? sid + " Updated Successfully" : sid + " Not Updated Successfully");

					} else {
						System.out.println("Invalid Option");
					}
					break;
				case 4:
					List<StudentMarks> list = controller.viewStudentMarks();
					if (list.size() > 0) {
						System.out.printf("%-7s%-15s%-15s%-15s%s\n", "sid", "sname", "java_Marks", "sql_Marks",
								"html_Marks");
						for (StudentMarks studentMarks : list) {
							System.out.printf("%-7s%-17s%-17s%-17s%s\n", studentMarks.getSid(), studentMarks.getSname(),
									studentMarks.getJava_Marks(), studentMarks.getSql_Marks(),
									studentMarks.getHtml_Marks());
						}

					} else {
						System.out.println("No Records Found");
					}
					break;
				default:
					System.out.println("Invalid Section");
				}
				System.out.println("Would you like to continue press 1");
				cont = s.nextInt();
			} while (cont == 1);

		} else {
			System.out.println("User id or Password incorrect");
		}
		System.out.println("Hi " + uid + " Would you like to change your Password then press 2 ");
		int cont = s.nextInt();
		s.nextLine();
		if (cont == 2) {
			System.out.println("Enter your user Id");
			String luid = s.nextLine();
			System.out.println("Enter your New Password");
			String npassword = s.nextLine();
			result = controller.changePassword(luid, npassword);
			System.out.println((result > 0) ? luid + " Your Password Changed Successfullly"
					: luid + " Your Password Not Changed Please check your User Id");
		}

		System.out.println("Done!!!!");
		s.close();
	}

}
